# README

- Generate a board of random letters.
- Identify all valid words (contained in the attached word list) in the board.
- Display results to the user.

### Install requirements

```
pip install -r requirements.txt
```

### Run

```
python3 wordsearch.py --help
```

Example:

```
python3 wordsearch.py --width 50 --height 50 -d words.txt
```
