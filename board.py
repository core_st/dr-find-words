""" Main board module.

Classes:

    Board

Functions:

    read_dictionary_from_file(string) -> []string

"""

import string
import random

class Board:
    """ Class representing game board. """
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.board = ['']*width

        for i in range(self.width):
            col = ['']*height
            for j in range(self.height):
                col[j] = random.choice(string.ascii_lowercase)
            self.board[i] = col

    def __str__(self):
        return "\n".join(["{}".format(" ".join(self.board[i])) for i in range(self.width)])

    def find_valid_words(self, dictionary):
        """ Returns valid words on board with length > 3 letters.

            Parameters:
                dictionary ([]string): List with words to validate against

            Returns:
                words ([]string): List with valid words on board

        """

        if len(self.board) == 0:
            print("Board is empty")
            return []

        words = []
        result = []
        for i in range(self.width):
            for j in range(self.height):
                words += self.find_symbol_combinations_from_position(i, j)

        for word in words:
            if word in dictionary:
                result.append(word)

        return result

    def find_symbol_combinations_from_position(self, i, j):
        """ Returns all possible combinations of symbols
            in all directions starting from [i][j] position.

            Parameters:
                i (int): Row on the board
                j (int): Column on the board

            Returns:
                words ([]string): Possible symbol combinations

        """
        words = []

        board = self.board
        # l -> r | r -> l
        left = j
        while left <= self.width:
            word = board[i][j:left]
            if len(word) > 2:
                words.append("".join(word))
                words.append("".join(word[::-1]))
            left+=1

        # transpose
        board = [*zip(*self.board)]
        left = j
        while left <= self.width:
            word = board[i][j:left]
            if len(word) > 2:
                words.append("".join(word))
                words.append("".join(word[::-1]))
            left+=1

        # diagonal left->right->down
        left, right = i, j
        word = []
        while left < self.width and right < self.height:
            word.append(board[left][right])
            if len(word) > 2:
                words.append("".join(word))
                words.append("".join(word[::-1]))
            left += 1
            right += 1

        # diagonal right -> left -> down
        left, right = i, j
        word = []
        while left >= 0 and right < self.height:
            word.append(board[left][right])
            if len(word) > 2:
                words.append("".join(word))
                words.append("".join(word[::-1]))
            left -= 1
            right += 1

        return words

def read_dictionary_from_file(path):
    """ Returns list of strings from file in local filesystem.

        Parameters:
            path (string): File path in local filesystem

        Returns:
            lines (dict): Dictionary with lines as keys

    """
    lines = {}
    reader = open(path, 'r')
    try:
        lines = {w.strip(): i for i, w in enumerate(reader.readlines())}
    finally:
        reader.close()
    return lines
