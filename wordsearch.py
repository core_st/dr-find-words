""" Search valid words in randomly generated 2D-array. """

import sys
import time
import click

import board

class GenericError(Exception):
    """ Returns generic error. """

@click.command()
@click.option('--width', '-w', type=int)
@click.option('--height', '-h', type=int)
@click.option('--dictionary-file', '-d', type=str)
def main(width, height, dictionary_file):
    """ Executes main program flow. """
    if width < 2 and height < 2:
        raise GenericError("Min board size is 1x2")

    if dictionary_file == "":
        raise GenericError("Dictionary file path can't be empty")

    dictionary = board.read_dictionary_from_file(dictionary_file)

    game = board.Board(width, height)

    print("Board [{}][{}]:\n{}\n\n".format(width, height, game))

    start_time = time.time()
    result = game.find_valid_words(dictionary)
    time_to_execute = time.time() - start_time
    if len(result) > 0:
        print("Result:\n{}\nTime:\n--- {} seconds ---".format(result, time_to_execute))
    else:
        print("No valid words found on board")
    print("\nTime:\n--- {} seconds ---".format(time_to_execute))


if __name__ == "__main__":
    try:
        main() # pylint: disable=no-value-for-parameter
    except GenericError as error:
        print(error)
        sys.exit(1)
