""" Test cases for board module. """

import unittest
import mock
import board

class TestBoard(unittest.TestCase):
    """ Tests methods of board class. """

    def test_board_init(self):
        """ Tests board initialization. """

        test_cases = [dict() for x in range(2)]
        test_cases[0] = {
            "name": "Test case for board with equal width and height",
            "data": {"width":3, "height": 3},
            "expected": {"width":3, "height": 3}
                }
        test_cases[1] = {
            "name": "Test case for board with different width and height",
            "data": {"width":5, "height": 3},
            "expected": {"width":5, "height": 3},
                }

        for test_case in test_cases:
            game = board.Board(test_case["data"]["width"], test_case["data"]["height"])
            self.assertEqual(game.width == test_case["expected"]["width"],
                    game.height == test_case["expected"]["height"])

    def test_find_valid_words(self):
        """ Tests lookup for all combinations of word
            on the board starting with given position.

        """

        test_cases = [dict() for x in range(2)]
        test_cases[0] = {
            "name": "Test case 1 - valid words exist",
            "data": [['r','o','w'],['o','w','l'],['q','w','e']],
            "dictionary": {"row": 0, "owl": 1, "word": 2},
            "expected": ["row", "owl"]
                }
        test_cases[1] = {
            "name": "Test case 2 - no valid words",
            "data": [['r','o','w'],['o','w','l'],['q','w','e']],
            "dictionary": {"fog": 0, "grog": 1, "word": 2},
            "expected": []
                }

        for test_case in test_cases:
            game = board.Board(3,3)
            game.board=test_case["data"]
            words = game.find_valid_words(test_case["dictionary"])
            self.assertEqual(words, test_case["expected"])

    def test_find_symbol_combinations_from_position(self):
        """ Tests lookup of all possible words combinations,
            starting from particular position.

        """

        test_cases = [dict() for x in range(2)]
        test_cases[0] = {
            "name": "Test case 1",
            "data": [['r','o','w', 'e'],['o','w','l', 'e'],['q','w','e', 'z'],
                ['g','w','y','j']],
            "position": {"row": 1, "col": 1},
            "expected": ['wle', 'elw', 'www', 'www', 'wej', 'jew']
                }
        test_cases[1] = {
            "name": "Test case 2",
            "data": [['r','o','w', 'e'],['o','w','l', 'e'],['q','w','e', 'z'],
                ['g','w','y','j']],
            "position": {"row": 3, "col":3},
            "expected": []
                }

        for test_case in test_cases:
            game = board.Board(4,4)
            game.board=test_case["data"]
            print(game)
            combinations = game.find_symbol_combinations_from_position(
                    test_case["position"]["row"], test_case["position"]["col"])
            self.assertEqual(combinations, test_case["expected"])


class TestHelperMethods(unittest.TestCase):
    """ Tests helper methods of board module. """

    def test_read_dictionary_from_file(self):
        """ Test reading lines from file. """

        test_cases = [dict() for x in range(2)]
        test_cases[0] = {
                "name": "Test case with empty file",
                "data": "",
                "expected": {}
                }
        test_cases[1] = {
                "name": "Test case with non-empty file",
                "data": "word1\nword2\nword3",
                "expected": {"word1":0, "word2": 1, "word3":2}
                }

        for test_case in test_cases:
            with mock.patch('builtins.open', mock.mock_open(read_data=test_case["data"])):
                with open("random"):
                    words = board.read_dictionary_from_file("random")
                    self.assertEqual(words, test_case["expected"])

if __name__ == '__main__':
    unittest.main()
